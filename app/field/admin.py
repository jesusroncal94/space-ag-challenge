from django.contrib import admin

from .models import FieldWorker

# Register your models here.

admin.site.register(FieldWorker)
