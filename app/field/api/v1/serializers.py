from django.contrib.auth.models import User, Group
from rest_framework import serializers

from ...models import FieldWorker


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ['url', 'username', 'email', 'groups']

class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ['url', 'name']

class FieldWorkerSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = FieldWorker
        fields = ['url', 'id', 'first_name', 'last_name', 'function', 'created_at', 'updated_at']
        read_only_fields = ['id', 'created_at', 'updated_at']
