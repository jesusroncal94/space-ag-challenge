import uuid
from django.db import models

# Create your models here.

class FieldWorker(models.Model):

    class Function(models.TextChoices):
        HARVEST = 'harvest', 'Harvest'
        PRUNING = 'pruning', 'Pruning'
        SCOUTING = 'scouting', 'Scouting'
        OTHER = 'other', 'Other'

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    function = models.CharField(
        max_length=8, choices=Function.choices, default=Function.OTHER)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['-created_at']

    def __repr__(self):
        return '<FieldWorker %s>' % self.id

    def __str__(self):
        return '%s %s' % (self.first_name, self.last_name)
