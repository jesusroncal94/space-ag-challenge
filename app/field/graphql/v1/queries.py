import graphene
from graphene_django import DjangoObjectType

from ...models import FieldWorker
from .types import FieldWorkerType


class Query(graphene.ObjectType):
    get_field_workers = graphene.List(
        FieldWorkerType,
        first=graphene.Int(), skip=graphene.Int())

    def resolve_get_field_workers(root, info, first=None, skip=None):
        field_workers = FieldWorker.objects.all()

        if skip:
            field_workers = field_workers[skip:]

        if first:
            field_workers = field_workers[:first]

        return field_workers 
