from graphene_django import DjangoObjectType

from ...models import FieldWorker


class FieldWorkerType(DjangoObjectType):
    
    class Meta:
        model = FieldWorker
        fields = '__all__'
        convert_choices_to_enum = False
