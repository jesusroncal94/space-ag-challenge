import graphene
from graphene_django import DjangoObjectType

from ...models import FieldWorker
from .types import FieldWorkerType


class FunctionEnum(graphene.Enum):
    Harvest = 'harvest'
    Pruning = 'pruning'
    Scouting = 'scouting'
    Other = 'other'


class CreateFieldWorker(graphene.Mutation):

    class Arguments:
        first_name = graphene.String(required=True)
        last_name = graphene.String(required=True)
        function = FunctionEnum()

    field_worker = graphene.Field(FieldWorkerType)

    @classmethod
    def mutate(cls, root, info, **kwargs):
        field_worker = FieldWorker()
        field_worker.first_name = kwargs['first_name']
        field_worker.last_name = kwargs['last_name']
        field_worker.function = kwargs['function']
        field_worker.save()

        return CreateFieldWorker(field_worker=field_worker)


class UpdateFieldWorker(graphene.Mutation):

    class Arguments:
        id = graphene.UUID()
        first_name = graphene.String(required=True)
        last_name = graphene.String(required=True)
        function = FunctionEnum()

    field_worker = graphene.Field(FieldWorkerType)

    @classmethod
    def mutate(cls, root, info, **kwargs):
        field_worker = FieldWorker.objects.get(pk=kwargs['id'])
        field_worker.first_name = kwargs['first_name']
        field_worker.last_name = kwargs['last_name']
        field_worker.function = kwargs['function']
        field_worker.save()

        return UpdateFieldWorker(field_worker=field_worker)


class DeleteFieldWorker(graphene.Mutation):
    ok = graphene.Boolean()

    class Arguments:
        id = graphene.UUID()

    @classmethod
    def mutate(cls, root, info, **kwargs):
        field_worker = FieldWorker.objects.get(pk=kwargs['id'])
        field_worker.delete()

        return DeleteFieldWorker(ok=True)


class Mutation(graphene.ObjectType):
    create_field_worker = CreateFieldWorker.Field()
    update_field_worker = UpdateFieldWorker.Field()
    delete_field_worker = DeleteFieldWorker.Field()
