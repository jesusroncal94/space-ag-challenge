from django.urls import include, path
from graphene_django.views import GraphQLView
from rest_framework import routers

from .api.v1 import VERSION as VERSION_API, views
from .graphql.v1 import VERSION as VERSION_GRAPHQL
from .graphql.v1.schema import schema


# API Router
router_api = routers.DefaultRouter()
# router.register(r'users', views.UserViewSet)
# router.register(r'groups', views.GroupViewSet)
router_api.register(r'field_workers', views.FieldWorkerViewSet)

# GraphQL Router
router_graphql = routers.DefaultRouter()
# router.register(r'users', views.UserViewSet)
# router.register(r'groups', views.GroupViewSet)
router_graphql.register(r'field_workers', views.FieldWorkerViewSet)

urlpatterns = [
    path('api/%s/' % VERSION_API, include(router_api.urls)),
    path(
        'graphql/%s/' % VERSION_GRAPHQL,
        GraphQLView.as_view(graphiql=True, schema=schema)),
]
