import uuid
from django.test import TestCase
from mixer.backend.django import mixer
from rest_framework.test import APIClient

from .models import FieldWorker

# Create your tests here.

class TestFieldWorkerAPIViews(TestCase):
    
    def setUp(self):
        self.client = APIClient()

    def test_list_field_workers(self):
        mixer.blend(FieldWorker, first_name='Jesus', last_name='Roncal', function='harvest')
        mixer.blend(FieldWorker, first_name='Jesus 2', last_name='Roncal 2', function='pruning')
        response = self.client.get('/api/v1/field_workers/')
        assert response.json() != None
        assert len(response.json()) >= 2
        assert response.status_code == 200

    def test_get_field_worker(self):
        uuid_for_testing = uuid.uuid4()
        mixer.blend(FieldWorker, id=uuid_for_testing, first_name='Jesus 3', last_name='Roncal 3', function='scouting')
        response = self.client.get('/api/v1/field_workers/%s/' % uuid_for_testing)
        assert response.json() != None
        assert response.status_code == 200
        assert response.json()['first_name'] == 'Jesus 3'
        assert response.json()['last_name'] == 'Roncal 3'
        assert response.json()['function'] == 'scouting'

    def test_create_field_worker(self):
        uuid_for_testing = uuid.uuid4()
        input_data = {
            'id': uuid_for_testing,
            'first_name': 'Jesus 4',
            'last_name': 'Roncal 4',
            'function': 'other'
        }
        response = self.client.post('/api/v1/field_workers/', data=input_data)
        assert response.json() != None
        assert response.status_code == 201
        assert response.json()['first_name'] == 'Jesus 4'
        assert response.json()['last_name'] == 'Roncal 4'
        assert response.json()['function'] == 'other'

    def test_create_field_worker_without_function(self):
        uuid_for_testing = uuid.uuid4()
        input_data = {
            'id': uuid_for_testing,
            'first_name': 'Jesus 5',
            'last_name': 'Roncal 5',
        }
        response = self.client.post('/api/v1/field_workers/', data=input_data)
        assert response.json() != None
        assert response.status_code == 201
        assert response.json()['first_name'] == 'Jesus 5'
        assert response.json()['last_name'] == 'Roncal 5'
        assert response.json()['function'] == 'other'

    def test_update_field_worker(self):
        uuid_for_testing = uuid.uuid4()
        mixer.blend(FieldWorker, id=uuid_for_testing, first_name='Jesus 6', last_name='Roncal 6', function='harvest')
        input_data = {
            'id': uuid_for_testing,
            'first_name': 'Jesus E.',
            'last_name': 'Roncal R.',
            'function': 'pruning'
        }
        response = self.client.put('/api/v1/field_workers/%s/' % uuid_for_testing, data=input_data)
        assert response.json() != None
        assert response.status_code == 200
        assert response.json()['first_name'] == 'Jesus E.'
        assert response.json()['last_name'] == 'Roncal R.'
        assert response.json()['function'] == 'pruning'

    def test_delete_field_worker(self):
        uuid_for_testing = uuid.uuid4()
        mixer.blend(FieldWorker, id=uuid_for_testing, first_name='Jesus 7', last_name='Roncal 7', function='scouting')
        response = self.client.delete('/api/v1/field_workers/%s/' % uuid_for_testing)
        assert response.status_code == 204
